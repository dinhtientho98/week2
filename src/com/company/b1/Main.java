package com.company.b1;

public class Main {

    public static void main(String[] args) {
        Stack inputStack = new Stack();
        Stack tempStack = new Stack();
        //
        inputStack.push(9);
        inputStack.push(43);
        inputStack.push(4);
        inputStack.push(23);
        inputStack.push(3);
        inputStack.push(34);
        inputStack.push(53);
        inputStack.push(5);
        inputStack.push(2);
        while (!inputStack.isEmpty()) {
            int tmp = inputStack.pop();
            while (!tempStack.isEmpty() && tempStack.peek() > tmp) {
                inputStack.push(tempStack.pop());
            }
            tempStack.push(tmp);
        }

        //print result
        while (!tempStack.isEmpty()) {
            System.out.print(tempStack.pop() + " ");
        }

    }
}
